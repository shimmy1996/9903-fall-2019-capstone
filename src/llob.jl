using Gadfly
using DataFrames
using QuadGK
using Optim
using SpecialFunctions
using Statistics

function gen_from_sparse_schedule(η, t, dt)
    # Generates a evenly spaced piece-wise constant schedule.
    # Assumes ascending t. It's ok for t to be longer than m by 1 element, in
    # which case the last value in m will be used for the final interval.
    schedule = DataFrame(t = minimum(t):dt:maximum(t))
    schedule[!, :η] .= 0.0
    schedule[!, :dt] .= dt
    for i in eachindex(η)
        schedule[schedule[!, :t] .>= t[i], :η] .= η[i]
    end
    schedule
end

function calc_price!(schedule)
    # Generates normalized price series zₜ for given schedule ηₜ.
    # Assume starting price is always 0.
    schedule[!, :z] .= 0.0
    for i in 2:size(schedule, 1)
        # Also assume that the trading rate in effect is the previous one.
        η = schedule[i - 1, :η]
        t = schedule[i, :t]
        function price_obj(zₜ)
            zₜ = zₜ[1]
            # Constraint price to be higher if buying (m > 0) and lower if
            # selling. This ignores more complex scenarios like m = [10, 1] and
            # is thus not applicable to more complex trading schedules.
            if (η > 0 && zₜ < schedule[i - 1, :z]) || (η < 0 && zₜ > schedule[i - 1, :z])
                return 10000^2 + (zₜ - schedule[i - 1, :z])^2
            end
            z_next = 0.0
            if i > 2
                @simd for j in 1:(i - 1)
                    @inbounds ηₛ = schedule[j, :η]
                    @inbounds zₛ = schedule[j, :z]
                    @inbounds s = schedule[j, :t]
                    @inbounds ds = schedule[j, :dt]
                    integrand = (
                        ηₛ
                        / √(2π * (t - s))
                        * exp(-(zₜ - zₛ)^2 / (2(t - s)))
                    )
                    # Trapzoidal rule except for the last step due to
                    # singularity.
                    if j == 1 || j == i - 1
                        z_next += integrand * ds / 2
                    else
                        z_next += integrand * ds
                    end
                end
            end
            # Use the linear price approximation for the last interval.
            @inbounds dt = schedule[i, :dt]
            @inbounds α = abs((zₜ - schedule[i - 1, :z]) / dt)
            # We assme the previous m is still in effect.
            if α == 0
                # Guard against cases where alpha = 0
                z_next += √(2dt / π) * η
            else
                z_next += η / α * erf(α * √(dt / 2))
            end
            (zₜ - z_next)^2
        end
        # Start guess from previous price.
        @inbounds z_guess = schedule[i - 1, :z]
        result = optimize(price_obj, [z_guess], LBFGS(); autodiff = :forward)
        @inbounds schedule[i, :z] = Optim.minimizer(result)[1]
    end
    schedule
end

function calc_cost!(schedule)
    # Computes cost of given execution schedule.
    # Note that η, the trading rate is assumed to last until beginning of next
    # time point.
    # Just do trapzoidal rule for now. Consider switching to FastGaussQuadrature.
    schedule[!, :c] .= 0.0
    for i in 1:(size(schedule, 1) - 1)
        schedule[i + 1, :c] = (
            schedule[i, :c]
            + schedule[i, :η] * schedule[i, :dt]
            * 0.5(schedule[i, :z] + schedule[i + 1, :z])
        )
    end
    schedule[end, :c]
end

function plot_schedule(schedule)
    # Plots given schedule, price, cost, and cuminative position.
    p1 = plot(
        schedule,
        x=:t, y=:η,
        Geom.step,
    );
    p2 = plot(
        schedule,
        x=:t, y=:z,
        Geom.line,
    );
    p3 = plot(
        schedule,
        x=:t, y=:c,
        Geom.line,
    );
    # Plot cumulative position, note that t=0 should have 0 position.
    cum_pos = circshift(cumsum(schedule.η .* schedule.dt), 1)
    cum_pos[1] = 0
    p4 = plot(
        schedule,
        x=:t, y=cum_pos,
        Geom.line,
        Guide.ylabel("position"),
    );
    vstack(hstack(p1, p4), hstack(p3, p2))
end

function plot_schedule_theo(schedule)
    # Plot schedule with theoretical solution.
    p2 = plot(
        schedule,
        x=:t, y=:z,
        Geom.line,
    );
    # Plot cumulative position, note that t=0 should have 0 position.
    cum_pos = circshift(cumsum(schedule.η .* schedule.dt), 1)
    cum_pos[1] = 0
    p4 = plot(
        schedule,
        x=:t, y=cum_pos,
        Geom.line,
        Guide.ylabel("position"),
    );
    # Avoid singularities at beginning and end.
    theo_schedule = DataFrame(
        t = (schedule.t[1:end - 1] .+ schedule.t[2:end]) ./ 2
    )
    theo_schedule[!, :η] = (theo_schedule.t .* (1 .- theo_schedule.t)).^(-1/4)
    theo_schedule[!, :η] .*= cum_pos[end] / (schedule.dt[1] .* sum(theo_schedule.η))
    p1 = plot(
        layer(schedule, x=:t, y=:η, Geom.step),
        layer(theo_schedule, x=:t, y=:η, Geom.line, Theme(default_color="Red")),
    );
    # For cost, compare with vwap execution.
    vwap = calc_vwap_price(cum_pos[end], schedule.t[end], schedule.dt[1])
    calc_cost!(vwap)
    p3 = plot(
        schedule,
        x=:t, y=:c,
        Geom.line,
        yintercept=[vwap.c[end]],
        Geom.hline(color="Red"),
    );
    vstack(hstack(p1, p4), hstack(p3, p2))
end

function find_best_schedule(n, Q, dt)
    # Find best execution schedule for Q shares with n evenly spaced slices.
    T = 1
    t = collect(0:(T/n):T)
    η_tot = n * Q / T
    function obj_func(η)
        # Impose the total trading size limit by computing the last interval η.
        η_all = copy(η)
        push!(η_all, η_tot - sum(η))
        trading_cost = calc_cost!(calc_price!(gen_from_sparse_schedule(η_all, t, dt)))
        trading_cost
    end
    # We only have n - 1 degrees of freedom due to total trading size limit.
    result = optimize(obj_func, ones(n - 1) .* (η_tot / n), LBFGS())
    best_schedule = Optim.minimizer(result)
    # Append the last m.
    push!(best_schedule, η_tot - sum(best_schedule))
    best_cost = Optim.minimum(result)
    println("Lowest cost achieved at $best_cost")
    gen_from_sparse_schedule(best_schedule, t, dt)
end

function find_best_schedule_capped(n, Q, dt, η_cap)
    # Find best execution schedule for Q shares with n evenly spaced slices.
    # Emposes a cap on η.
    T = 1
    t = collect(0:(T/n):T)
    η_tot = n * Q / T
    # TODO: add sanity check for η_cap.
    function obj_func(η)
        # Impose the total trading size limit by computing the last interval η.
        η_all = copy(η)
        push!(η_all, η_tot - sum(η))
        trading_cost = calc_cost!(calc_price!(gen_from_sparse_schedule(η_all, t, dt)))
        # Also penalize η that are too high.
        # Positive penalty for now - need penalize negative η as well.
        # Log barrier can be breached by algo and cause invalid results.
        penalty = sum(exp.(η_all[η_all .> η_cap]))
        trading_cost + penalty
    end
    # We only have n - 1 degrees of freedom due to total trading size limit.
    result = optimize(obj_func, ones(n - 1) .* (η_tot / n), LBFGS())
    best_schedule = Optim.minimizer(result)
    # Append the last η.
    push!(best_schedule, η_tot - sum(best_schedule))
    best_cost = Optim.minimum(result)
    println("Lowest cost achieved at $best_cost")
    gen_from_sparse_schedule(best_schedule, t, dt)
end

function vwap_impact(η)
    # Computes the order impact accoring to Bouchaud (12).
    # Define rho = m_0 / J.
    function impact_obj(A)
        A = A[1]
        integral = quadgk(
            u -> exp(-A^2 * (1 - √u) / 2(1 + √u)) / √(2π * (1 - u)),
            0, 1, rtol=1e-5)[1]
        (A - η * integral)^2
    end
    result = optimize(impact_obj, zeros(1), LBFGS())
    Optim.minimizer(result)[1]
end

function calc_vwap_price(Q, T, dt)
    # Generates price profile using closed form solution for vwap scheduls.
    schedule = gen_from_sparse_schedule([Q / T], [0, T], dt)
    # The impact is given by Bouchaud (12).
    A = vwap_impact(Q / T)
    schedule[!, :z] = sqrt.(schedule[!, :t]) * A
    schedule
end
